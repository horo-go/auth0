package auth0

import (
	"encoding/json"
	"gopkg.in/square/go-jose.v2"
	"net/http"
	"strings"
	"sync"
)

type keyStore struct {
	store map[string]jose.JSONWebKey
	sync.RWMutex
}

type JWKS struct {
	Keys []jose.JSONWebKey `json:"keys"`
}

func newKeyStore() keyStore {
	return keyStore{
		store: make(map[string]jose.JSONWebKey),
	}
}

func (auth *Auth0) getKey(id string) (jose.JSONWebKey, error) {
	auth.keyStore.RLock()
	searchedKey, exist := auth.keyStore.store[id]
	auth.keyStore.RUnlock()

	if !exist {
		if keys, err := auth.downloadKeys(); err != nil {
			return jose.JSONWebKey{}, err
		} else {
			auth.keyStore.Lock()
			for _, key := range keys {
				auth.keyStore.store[key.KeyID] = key

				if key.KeyID == id {
					searchedKey = key
					exist = true
				}
			}
			auth.keyStore.Unlock()
		}
	}

	if exist {
		return searchedKey, nil
	}

	return jose.JSONWebKey{}, ErrNoKeyFound
}

func (auth *Auth0) downloadKeys() ([]jose.JSONWebKey, error) {
	r, err := http.Get(auth.cfg.JWKSURI)

	if err != nil {
		return []jose.JSONWebKey{}, err
	}
	defer r.Body.Close()

	if header := r.Header.Get("Content-Type"); !strings.HasPrefix(header, "application/json") {
		return []jose.JSONWebKey{}, ErrInvalidContentType
	}

	var jwks = JWKS{}
	err = json.NewDecoder(r.Body).Decode(&jwks)

	if err != nil {
		return []jose.JSONWebKey{}, err
	}

	if len(jwks.Keys) < 1 {
		return []jose.JSONWebKey{}, ErrNoKeyFound
	}

	return jwks.Keys, nil
}
