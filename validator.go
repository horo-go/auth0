package auth0

import (
	"errors"
	"time"

	"gopkg.in/square/go-jose.v2/jwt"
)

var (
	ErrTokenNotFound      = errors.New("token not found")
	ErrInvalidAlgorithm   = errors.New("signing algorithm is supported")
	ErrNoJWTHeaders       = errors.New("no headers in the token")
	ErrInvalidContentType = errors.New("should have a JSON content type for JWKS endpoint")
	ErrNoKeyFound         = errors.New("no Keys has been found")
)

type Claims struct {
	jwt.Claims
	Scope string `json:"scope"`
}

func (auth *Auth0) GetClaims(raw string, signingMethod signatureAlgorithm) (*Claims, error) {
	if raw == "" {
		return nil, ErrTokenNotFound
	}

	token, err := jwt.ParseSigned(raw)
	if err != nil {
		return nil, err
	}

	if len(token.Headers) < 1 {
		return nil, ErrNoJWTHeaders
	}

	header := token.Headers[0]
	if header.Algorithm != string(signingMethod) {
		return nil, ErrInvalidAlgorithm
	}

	claims := &Claims{}
	var key interface{}
	if signingMethod == RS256 {
		key, err = auth.getKey(header.KeyID)
		if err != nil {
			return nil, err
		}
	} else {
		key = auth.cfg.Secret
	}

	if err = token.Claims(key, claims); err != nil {
		return nil, err
	}

	expected := auth.expectedClaim.WithTime(time.Now())
	err = claims.Validate(expected)

	if err != nil {
		return nil, err
	}

	return claims, nil
}
