package auth0

import (
	"context"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/horo-go/errorr"
	"gitlab.com/horo-go/horo"
	"gopkg.in/square/go-jose.v2/jwt"
)

type signatureAlgorithm string

const (
	RS256 = signatureAlgorithm("RS256")
	HS256 = signatureAlgorithm("HS256")
)

type Config struct {
	Audience       []string
	Issuer         string
	Secret         []byte
	JWKSURI        string
	SigningMethod  signatureAlgorithm
	TokenExtractor TokenExtractor
	ErrorLog       *log.Logger
}

type Auth0 struct {
	cfg           *Config
	expectedClaim jwt.Expected
	keyStore      keyStore
}

func New(cfg Config) *Auth0 {
	auth := new(Auth0)
	auth.cfg = &cfg
	auth.keyStore = newKeyStore()
	auth.expectedClaim = jwt.Expected{
		Issuer:   cfg.Issuer,
		Audience: cfg.Audience,
	}
	if cfg.ErrorLog == nil {
		cfg.ErrorLog = log.New(os.Stderr, "HORO-AUTH0  ", log.LstdFlags)
	}
	return auth
}

// Handler return a Horo Handler middleware
func (auth *Auth0) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		a := new(horo.Authentication)
		a.AccessToken = auth.cfg.TokenExtractor(r)
		claims, err := auth.GetClaims(a.AccessToken, auth.cfg.SigningMethod)
		if err != nil {
			a.Authenticated = false
		} else {
			a.Authenticated = true
			a.Subject = claims.Subject
			a.Authorities = strings.Split(claims.Scope, " ")
		}
		r = r.WithContext(context.WithValue(r.Context(), horo.AuthKey, a))
		next.ServeHTTP(w, r)
	})
}

func getAuthentication(ctx context.Context) *horo.Authentication {
	au, ok := ctx.Value(horo.AuthKey).(*horo.Authentication)
	if !ok {
		panic(errorr.ErrNoAuthentication)
	}
	return au
}

func (auth *Auth0) Authenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if getAuthentication(r.Context()).Authenticated {
			next.ServeHTTP(w, r)
			return
		}
		http.Error(w, "request is not authenticated", http.StatusUnauthorized)
	})
}

func (auth *Auth0) HasAuthority(authority string) horo.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			au := getAuthentication(r.Context())
			if au.Authenticated && au.HasAuthority(authority) {
				next.ServeHTTP(w, r)
				return
			}
			http.Error(w, "request is not permitted", http.StatusForbidden)
		})
	}
}
