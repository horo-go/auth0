package auth0

import (
	"net/http"
	"strings"
)

type TokenExtractor func(r *http.Request) string


func ExtractTokenFromCookie(cookieName string) TokenExtractor {
	return TokenExtractor(func(r *http.Request) string {
		cookie, err := r.Cookie(cookieName)
		if err != nil {
			return ""
		}
		return cookie.Value
	})
}

func ExtractTokenFromHeader() TokenExtractor {
	return TokenExtractor(func(r *http.Request) string {
		raw := ""
		if h := r.Header.Get("Authorization"); len(h) > 7 && strings.EqualFold(h[0:7], "BEARER ") {
			raw = h[7:]
		}
		return raw
	})
}

func ExtractTokenFromCookieOrHeader(cookieName string) TokenExtractor {
	return TokenExtractor(func(r *http.Request) string {
		cookie, err := r.Cookie(cookieName)
		if err != nil {
			raw := ""
			if h := r.Header.Get("Authorization"); len(h) > 7 && strings.EqualFold(h[0:7], "BEARER ") {
				raw = h[7:]
			}
			return raw
		}
		return cookie.Value
	})
}
